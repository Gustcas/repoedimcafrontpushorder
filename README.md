Test project for Edimca point of sale

The front end was made with the Node v12.18.3 version with React Js and a Primefaces React template for the visual

Getting started

First you must download all the dependencies of the project

With the command

npm install


It downloads all Node modules so that the project can run.

Second, once the node dependencies are downloaded, the application must be running


as the command

npm start

The default user is
user: admin
password: admin

Note: before running the project you must have installed the node version that is indicated and validate if the ports by default are not occupied
