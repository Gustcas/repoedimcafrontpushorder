import React, { useEffect, useRef, useState } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { metodoRest } from "../../src/middleware/api/metodoRest";
import { SERVICIO_REST_AUTORIZACONES } from "../../src/middleware/endponit/ApiRestEnpoint";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import "./OverlayPanelDemo.css";
export const ListaOrden = () => {
    const pathListaOrden = SERVICIO_REST_AUTORIZACONES.listaOrden;
    const pathListaOrdenDetalle = SERVICIO_REST_AUTORIZACONES.listaOrdenDetalle;
    const [valuelistaOrden, setlistaOrden] = useState([]);
    const [valuelistaOrdenDetalle, setlistaOrdenDetalle] = useState([]);
    const [detalleDialog, setdetalleDialog] = useState(false);

    useEffect(() => {
        setlistaOrdenDetalle([]);
        setlistaOrden([]);
        listarOrden();
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    const actionBodyTemplate = (rowData) => {
        //console.log(rowData);
        return (
            <React.Fragment>
                <Button label="Ver detalle" className="p-button-rounded p-button-success" onClick={() => verDetalle(rowData)} />
            </React.Fragment>
        );
    };
    const verDetalle = (product) => {
        // console.log(product.idorden);
        metodoRest.metodoGetData(pathListaOrdenDetalle + product.idorden).then((values) => {
            setlistaOrdenDetalle(values);
            return values;
        });
        setdetalleDialog(true);
    };
    const hideDialog = () => {
        setdetalleDialog(false);
    };
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
        </React.Fragment>
    );

    async function listarOrden() {
        return await metodoRest.metodoGetData(pathListaOrden).then((values) => {
            setlistaOrden(values);
            return values;
        });
    }

    const [layout, setLayout] = useState("list");

    const handleResize = () => {
        if (window.innerWidth <= 768) {
            setLayout("grid");
        } else {
            setLayout("list");
        }
    };

    const List = () => {
        return (
            <div>
                <div className="card">
                    <DataTable value={valuelistaOrden} dataKey="idorden">
                        <Column field="idorden" header="idorden"></Column>
                        <Column field="fechacreacionorden" header="Fecha Orden"></Column>
                        <Column field="nombrecliente" header="Nombre Cliente"></Column>
                        <Column field="cedulacliente" header="Cedula Cliente"></Column>
                        <Column field="direccioncliente" header="Direccion Cliente"></Column>
                        <Column field="estado" header="estado"></Column>
                        <Column body={actionBodyTemplate}></Column>
                    </DataTable>
                </div>
            </div>
        );
    };

    const Grid = ({ valuelistaOrden }) => {
        //console.log("valuelistaOrden", valuelistaOrden);
        return (
            <div className="grid">
                {valuelistaOrden.map((item) => (
                    <div key={item.idorden} className="grid-item">
                        <div className="grid-item-container">
                            <div className="grid-item-text">
                                <b className="grid-item-text-p">Cliente: </b>
                                <label className="grid-item-text-l">{item.nombrecliente}</label>
                            </div>
                            <div className="grid-item-text">
                                <b className="grid-item-text-p">Fecha: </b>
                                <label className="grid-item-text-l"> {item.fechacreacionorden}</label>
                            </div>
                            <div className="grid-item-text">
                                <b className="grid-item-text-p">Cedula: </b>
                                <label className="grid-item-text-l">{item.cedulacliente}</label>
                            </div>
                            <div className="grid-item-text">
                                <b className="grid-item-text-p">Direccion: </b>
                                <label className="grid-item-text-l">{item.direccioncliente}</label>
                            </div>
                            <div className="grid-item-btn">{actionBodyTemplate(item)}</div>
                        </div>
                    </div>
                ))}
            </div>
        );
    };

    return (
        <div className="App">
            {layout === "list" && <List />}
            {layout === "grid" && <Grid valuelistaOrden={valuelistaOrden} />}
            <Dialog visible={detalleDialog} style={{ width: "500px" }} header="Detalle Productos" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                <DataTable value={valuelistaOrdenDetalle}>
                    <Column field="nombreproducto" header="Producto"></Column>
                    <Column field="cantidad" header="cantidad"></Column>
                    <Column field="total" header="total"></Column>
                </DataTable>
            </Dialog>
        </div>
    );
};
