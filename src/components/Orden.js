import React, { useEffect, useRef, useState } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { InputTextarea } from "primereact/inputtextarea";
import { Dropdown } from "primereact/dropdown";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Calendar } from "primereact/calendar";
import { Toast } from "primereact/toast";
import { metodoRest } from "../../src/middleware/api/metodoRest";
import { SERVICIO_REST_AUTORIZACONES } from "../../src/middleware/endponit/ApiRestEnpoint";
import { OverlayPanel } from "primereact/overlaypanel";
import "./OverlayPanelDemo.css";
export const Orden = () => {
    const toast = useRef();
    const [valueFechaOrden, setFechaOrden] = useState("");
    const [valueNombreCliente, setNombreCliente] = useState("");
    const [valueCedulaCliente, setCedulaCliente] = useState("");
    const [valueDirreccionCliente, setDirreccionCliente] = useState("");
    const [valueProductos, setProductos] = useState([]);
    const [valueProductosDetalle, setProductosDetalle] = useState(null);
    let [valueProductosDetalleData, setProductosDetalleData] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const op = useRef(null);
    const pathProductos = SERVICIO_REST_AUTORIZACONES.listarProductos;
    const pathOrdenCabecera = SERVICIO_REST_AUTORIZACONES.crearOrdenCabecera;
    const pathOrdenDetalle = SERVICIO_REST_AUTORIZACONES.crearOrdenDetalle;
    const isMounted = useRef(false);
    const [valueIva, setIva] = useState(0);
    const [valueTotal, setTotal] = useState(0);
    useEffect(() => {
        setFechaOrden("");
        setNombreCliente("");
        setCedulaCliente("");
        setDirreccionCliente("");
        setProductos([]);
        setProductosDetalle([]);
        setProductosDetalleData([]);
        dataProductos();
        if (isMounted.current) {
            op.current.hide();
            toast.current.show({ severity: "info", summary: "Product Selected", detail: selectedProduct.name, life: 3000 });
        }
    }, []);

    const precioventaBody = (rowData) => {
        return formatCurrency(rowData.precioventa);
    };
    const formatCurrency = (value) => {
        return value.toLocaleString("en-US", { style: "currency", currency: "USD" });
    };
    const onProductSelect = (e) => {
        setSelectedProduct(e.value);
        let _products = [...valueProductosDetalle];
        _products.push(e.value);
        setProductosDetalle(_products);
    };
    const inputTextEditor = (productKey, props, field) => {
        return <InputText type="text" value={props.rowData[field]} onChange={(e) => onEditorValueChange(productKey, props, e.target.value)} />;
    };
    const codeEditor = (productKey, props) => {
        return inputTextEditor(productKey, props, "code");
    };

    const onEditorValueChange = (productKey, props, value) => {
        let updatedProducts = [...props.value];
        valueProductosDetalleData = [...props.value];
        let iva = value * 0.12;
        let costo = updatedProducts[props.rowIndex]["precioventa"];
        let valortotal = costo * value + iva;
        updatedProducts[props.rowIndex][props.field] = value;
        updatedProducts[props.rowIndex]["iva"] = value * 0.12;
        updatedProducts[props.rowIndex]["total"] = valortotal;
        setProductosDetalleData(updatedProducts);
        setIva(iva);
        setTotal(valortotal);
    };
    return (
        <div className="p-grid">
            <Toast ref={toast} />
            <div className="p-col-12">
                <div className="card">
                    <h5>Orden</h5>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-6">
                            <label htmlFor="firstname2">Fecha Orden</label>
                            <InputText type="date" value={valueFechaOrden} onChange={(e) => setFechaOrden(e.target.value)} mask="99/99/9999" />
                        </div>
                        <div className="p-field p-col-12 p-md-6">
                            <label htmlFor="lastname2">Nombre Cliente</label>
                            <InputText id="lastname2" type="text" onInput={(e) => setNombreCliente(e.target.value)} />
                        </div>
                        <div className="p-field p-col-12 p-md-6">
                            <label htmlFor="city">Cedula Cliente</label>
                            <InputText id="city" type="text" onInput={(e) => setCedulaCliente(e.target.value)} />
                        </div>
                        <div className="p-field p-col-12 p-md-6">
                            <label htmlFor="zip">Direccion Cliente</label>
                            <InputText id="zip" type="text" onInput={(e) => setDirreccionCliente(e.target.value)} />
                        </div>
                        <div className="p-field p-col-5 ">
                            <Button icon="pi pi-search" label="Agregar Producto" onClick={(e) => op.current.toggle(e)} aria-haspopup aria-controls="overlay_panel" className="select-product-button"></Button>

                            <OverlayPanel ref={op} showCloseIcon id="overlay_panel" style={{ width: "450px" }} className="overlaypanel-demo">
                                <DataTable value={valueProductos} paginator rows={5} selection={selectedProduct} onSelectionChange={onProductSelect} selectionMode="single" dataKey="idproducto">
                                    <Column field="nombreproducto" header="Producto"></Column>
                                    <Column field="precioventa" header="Costo" sortable body={precioventaBody}></Column>
                                    <Column field="stock" header="Stock"></Column>
                                </DataTable>
                            </OverlayPanel>
                        </div>
                        <div className="p-field p-col-5 ">
                            <Button label="Generar Orden" onClick={() => postOrden()}></Button>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <DataTable value={valueProductosDetalle} editMode="cell" dataKey="idordendetalle">
                        <Column field="idproducto" header="Cod. Producto"></Column>
                        <Column field="nombreproducto" header="Producto"></Column>
                        <Column field="precioventa" header="Costo" onchange={(props) => codeEditor("products1", props)}></Column>
                        <Column field="cantidad" header="Cantidad" editor={(props) => codeEditor("products1", props)}></Column>
                        <Column field="iva" header="Iva" onchange={(props) => codeEditor("products1", props)} value={setIva}></Column>
                        <Column field="total" header="Total" onchange={(props) => codeEditor("products1", props)} value={setTotal}></Column>
                    </DataTable>
                </div>
            </div>
        </div>
    );
    //mentodo para Mensaje tipo Toast local({tipo,tituloAlerta,mensajeAlerta})
    // recibe como parametros tipo{info,success,warn,error},tituloAlerta,mensajeAlerta todo en string
    function mensajeToast(tipo, tituloAlerta, mensajeAlerta) {
        return toast.current.show({ severity: tipo, summary: tituloAlerta, detail: mensajeAlerta, life: 3000 });
    }

    /*Declaración de Metodos/Funciones de la logicas del nuevo desarrollo*/
    async function dataProductos() {
        return await metodoRest.metodoGetData(pathProductos).then((values) => {
            setProductos(values);
            return values;
        });
    }

    async function postOrden() {
        let idOrden;
        let registroGurdados = false;
        if ((valueFechaOrden !== "") & (valueNombreCliente !== "") & (valueCedulaCliente !== "") & (valueDirreccionCliente !== "") & (valueProductosDetalleData.length > 0)) {
            let ordenCabecera = {
                fechacreacionorden: valueFechaOrden,
                nombrecliente: valueNombreCliente,
                cedulacliente: valueCedulaCliente,
                direccioncliente: valueDirreccionCliente,
                estado: "A",
            };
            let responPost = await metodoRest.metodoPostData(pathOrdenCabecera, ordenCabecera);
            if (responPost.status === 200) {
                idOrden = responPost.data.idorden;
                valueProductosDetalleData.forEach(async (element) => {
                    let OrdenDetalle = {
                        idproducto: element.idproducto,
                        idorden: idOrden,
                        cantidad: element.cantidad,
                        iva: element.iva,
                        total: element.total,
                        estado: "A",
                    };
                    let responPost = await metodoRest.metodoPostData(pathOrdenDetalle, OrdenDetalle);
                    if (responPost.status === 200) {
                        //console.log.log('post del detalle ',responPost.status)
                    }
                });
                registroGurdados = true;
            }
            //console.log.log(registroGurdados)
            if (registroGurdados == false) {
                mensajeToast("error", "Error..!!", "Orden no creada");
            } else {
                mensajeToast("success", "Exito..!!", "Orden Creada");
            }
        } else {
            mensajeToast("warn", "Alvertencia..!", "Por favor llene todos los cammpos y añada al menos un producto");
        }
    }
};
