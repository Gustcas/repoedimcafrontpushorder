import React, { useEffect, useRef, useState } from "react";
import { InputText } from "primereact/inputtext";
import { Route, useHistory } from "react-router-dom";
import { Button } from "primereact/button";
import { metodoRest } from "../../src/middleware/api/metodoRest";
import { SERVICIO_REST_AUTORIZACONES } from "../../src/middleware/endponit/ApiRestEnpoint";
import { Toast } from "primereact/toast";
export const Producto = () => {
    const pathCrearProducto = SERVICIO_REST_AUTORIZACONES.crearProducto;
    const toast = useRef();
    const [valueNombrePorducto, setNombrePorducto] = useState("");
    const [valuePrecio, setPrecio] = useState(0.0);
    const [valueStock, setStock] = useState(0);
    const history = useHistory();
    useEffect(() => {
        setNombrePorducto("");
        setPrecio();
        setStock();
    }, []);

    return (
        <div className="p-grid p-jc-center">
            <Toast ref={toast} />
            <div className="p-col-12 p-md-6 ">
                <div className="card p-fluid">
                    <h5>Producto</h5>
                    <div className="p-field">
                        <label htmlFor="name1">Nombre Producto</label>
                        <InputText id="NombrePorducto" type="text" onInput={(e) => setNombrePorducto(e.target.value)} />
                    </div>
                    <div className="p-field">
                        <label htmlFor="email1">Precio</label>
                        <InputText id="Precio" type="text" onInput={(e) => setPrecio(e.target.value)} />
                    </div>
                    <div className="p-field">
                        <label htmlFor="age1">Stock Inical</label>
                        <InputText id="Stock" type="text" onInput={(e) => setStock(e.target.value)} />
                    </div>
                    <Button label="Guardar" style={{ height: "50px" }} onClick={() => postPorducto()}></Button>
                </div>
            </div>
        </div>
    );

    //mentodo para Mensaje tipo Toast local({tipo,tituloAlerta,mensajeAlerta})
    // recibe como parametros tipo{info,success,warn,error},tituloAlerta,mensajeAlerta todo en string
    function mensajeToast(tipo, tituloAlerta, mensajeAlerta) {
        return toast.current.show({ severity: tipo, summary: tituloAlerta, detail: mensajeAlerta, life: 3000 });
    }

    async function postPorducto() {
        if ((valueNombrePorducto !== "") & (valuePrecio !== "") & (valueStock !== "")) {
            let newProductoData = {
                nombreproducto: valueNombrePorducto,
                precioventa: valuePrecio,
                stock: valueStock,
                estado: "A",
            };
            //console.log.log(newProductoData)
            let responPost = await metodoRest.metodoPostData(pathCrearProducto, newProductoData);
            if (responPost.status === 200) {
                mensajeToast("success", "Exito!", "Producto Guardado..!!");
                document.getElementById("NombrePorducto").value = "";
                document.getElementById("Precio").value = "";
                document.getElementById("Stock").value = "";
            } else {
                mensajeToast("warn", "Error", "El producto no se guardo en la base de datos ");
            }
        } else {
            mensajeToast("warn", "Error", "Por favor ingrese informaciòn ");
        }
    }
};
