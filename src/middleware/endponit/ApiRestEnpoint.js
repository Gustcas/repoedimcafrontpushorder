/*
Variables constantes que llevan los endpoint para consumir lo servicios rest
el contexto principal para los servicios es = "http://localhost:3304/api/" que mapea la ip local 
*/
//const BASEURLAUTORIZACIONES = "http://localhost:3304/api/"; //test
const BASEURLAUTORIZACIONES = "https://salesorderbegp.onrender.com/api/"; //test
export const SERVICIO_REST_AUTORIZACONES = {
    validarLogin: BASEURLAUTORIZACIONES + "validarLogin/",
    crearProducto: BASEURLAUTORIZACIONES + "crearProducto",
    listarProductos: BASEURLAUTORIZACIONES + "allPorducto",
    crearOrdenCabecera: BASEURLAUTORIZACIONES + "crearOrdenCabecera",
    crearOrdenDetalle: BASEURLAUTORIZACIONES + "crearOrdenDetalle",
    listaOrden: BASEURLAUTORIZACIONES + "listaOrden",
    listaOrdenDetalle: BASEURLAUTORIZACIONES + "listaOrdenDetalle/",
    login: BASEURLAUTORIZACIONES + "login",
};
