import React, { useEffect, useRef, useState } from "react";
import { Button } from "primereact/button";
import { Divider } from "primereact/divider";
import { InputText } from "primereact/inputtext";
import { Password } from "primereact/password";
import { metodoRest } from "../../middleware/api/metodoRest";
import { SERVICIO_REST_AUTORIZACONES } from "../../middleware/endponit/ApiRestEnpoint";
import { Toast } from "primereact/toast";
import { Route, useHistory } from "react-router-dom";
import { FormLayoutDemo } from "../../../src/components/FormLayoutDemo";
import App from "../../../src/App";
import { Dialog } from "primereact/dialog";
import "./styles.css";
import { observer } from "mobx-react";
import { useDataStore } from "../../data/DataStoreContext";
export const Login = observer((props) => {
    const toast = useRef();
    const [valueUsuario, setValueUsuario] = useState("");
    const [valuePassword, setValuePassword] = useState("");
    const [dialog, setDialog] = useState(false);
    const pathValidarLoginOld = SERVICIO_REST_AUTORIZACONES.validarLogin;
    const pathValidarLogin = SERVICIO_REST_AUTORIZACONES.login;
    const history = useHistory();
    /*
    Store
    */
    const dataStore = useDataStore();

    /* instancias iniciales de Hooks propios de react js*/
    useEffect(() => {
        setValuePassword("");
        setValueUsuario("");
    }, []);
    //mentodo para Mensaje tipo Toast local({tipo,tituloAlerta,mensajeAlerta})
    // recibe como parametros tipo{info,success,warn,error},tituloAlerta,mensajeAlerta todo en string
    function mensajeToast(tipo, tituloAlerta, mensajeAlerta) {
        return toast.current.show({ severity: tipo, summary: tituloAlerta, detail: mensajeAlerta, life: 3000 });
    }
    /*Declaración de Metodos/Funciones de la logicas del nuevo desarrollo*/
    async function validarLogin() {
        if (valueUsuario !== "" && valuePassword !== "") {
            let exiteUsurio;
            let loginDto = { username: valueUsuario, password: valuePassword };
            await metodoRest.metodoPostData(pathValidarLogin, loginDto).then((valid) => {
                if (valid.data && valid.data.logged) {
                    mensajeToast("success", "Exito!", valid.data.message);
                    //console.log.log("validLogin ", valid.data.username);
                    exiteUsurio = valid.data.logged;
                    //console.log.log(exiteUsurio);
                    dataStore.setAuthPrincipalUser(valid.data);
                    localStorage.setItem("login", true);
                    props.setIsLoggedIn(true);
                } else {
                    //console.log.log(exiteUsurio);
                    setValuePassword("");
                    setValueUsuario("");
                    mensajeToast("warn", "Error", valid.data.message);
                }
            });
        } else {
            mensajeToast("error", "Error", "Por favor ingresar el usuario/passorwd");
            setValuePassword("");
            setValueUsuario("");
        }
    }

    function handleSignUpClick() {
        setDialog(true);
    }

    function handleForgotPasswordClick() {
        setDialog(true);
    }

    const hideDialog = () => {
        setDialog(false);
    };

    return (
        <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", margin: "auto", background: "#f69f43" }}>
            <Toast ref={toast} />
            <div className="m-card">
                <div className="p-grid">
                    <img src="assets/layout/images/EdimcaLogov2.png" alt="Edimca Logo" className="logo" />
                    <div className="form-fields">
                        <div className="p-field">
                            <span className="p-float-label">
                                <InputText id="username" type="text" placeholder="Username" style={{ height: "50px" }} onInput={(e) => setValueUsuario(e.target.value)} />
                            </span>
                        </div>
                        <div className="p-field">
                            <span className="p-float-label">
                                <Password id="password" type="password" placeholder="Password" style={{ height: "50px" }} onInput={(e) => setValuePassword(e.target.value)} />
                            </span>
                        </div>
                        <Button label="Login" className="p-button-raised" onClick={validarLogin} />
                        <br></br>
                        <br></br>
                        <div className="p-field">
                            <Button label="Sign up" link style={{ color: "white", backgroundColor: "transparent ", borderColor: "transparent" }} onClick={() => handleSignUpClick()} />
                        </div>
                        <div className="p-field">
                            <Button label="Olvidaste la contraseña ?" link style={{ color: "white", backgroundColor: "transparent", borderColor: "transparent" }} onClick={() => handleForgotPasswordClick()} />
                        </div>
                    </div>
                </div>
            </div>
            <Dialog visible={dialog} style={{ width: "500px" }} header="Modal" modal className="p-fluid" onHide={hideDialog}>
                <div className="flex flex-column px-8 py-5 gap-4" style={{ borderRadius: "12px", backgroundImage: "radial-gradient(circle at left top, var(--primary-400), var(--primary-700))" }}>
                    <div className="inline-flex flex-column gap-2">
                        <InputText id="username" label="Username" className="p-inputtext"></InputText>
                    </div>
                    <div className="inline-flex flex-column gap-2">
                        <InputText id="password" label="Password" className="p-inputtext" type="password"></InputText>
                    </div>
                    <div className="flex align-items-center gap-2">
                        <Button label="Sign-In" text className="p-button-raised"></Button>
                    </div>
                    <div className="flex align-items-center gap-2">
                        <Button label="Cancel" text className="p-button-raised"></Button>
                    </div>
                </div>
            </Dialog>
        </div>
    );
});
