import axios from "axios";

export class metodoRest {
    // metodo uno para cosumir rest
    static async metodoGetData(pathApi) {
        //console.log.log(pathApi)
        const respone = await fetch(pathApi);
        if (respone.status === 200) {
            const responJson = await respone.json();
            return responJson;
        } else {
            if (respone.status === 400) {
                const responJson = respone.status;
                return responJson;
            }
        }
    }
    // metodo dos para consumir rest con la libreria axios
    static getAll = async (pathApi, access_token) => {
        return await axios
            .get(pathApi, {
                headers: {
                    Authorization: `${access_token}`,
                },
            })
            .then((res) => res.data)
            .catch((e) => {
                return e;
            });
    };

    /*metodo post para enviar data al rest*/
    static metodoPostData = async (pahtApi, dataenviar) => {
        return await axios({
            method: "post",
            url: pahtApi,
            data: dataenviar,
        })
            .then((response) => {
                return response;
            })
            .catch((error) => {
                return error;
            });
    };
}
