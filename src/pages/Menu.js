import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import { Route, useHistory } from "react-router-dom";
import { CSSTransition } from "react-transition-group";

import { AppTopbar } from "../../src/AppTopbar";
import { AppFooter } from "../../src/AppFooter";
import { AppMenu } from "../../src/AppMenu";
import { AppProfile } from "../../src/AppProfile";
import { AppConfig } from "../../src/AppConfig";

import { EmptyPage } from "../../src/pages/EmptyPage";
import { Producto } from "../../src/components/Producto";
import { Orden } from "../../src/components/Orden";
import { ListaOrden } from "../../src/components/ListaOrden";
import { FormLayoutDemo } from "../../src/components/FormLayoutDemo";

import PrimeReact from "primereact/api";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "prismjs/themes/prism-coy.css";
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import "@fullcalendar/timegrid/main.css";
import "../../src/layout/flags/flags.css";
import "../../src/layout/layout.scss";
import "../../src/App.scss";
import { useDataStore } from "../data/DataStoreContext";
export const Menu = ({ setIsLoggedIn }) => {
    const [layoutMode, setLayoutMode] = useState("overlay");
    const [layoutColorMode, setLayoutColorMode] = useState("dark");
    const [inputStyle, setInputStyle] = useState("outlined");
    const [ripple, setRipple] = useState(false);
    const [sidebarActive, setSidebarActive] = useState(true);
    const sidebar = useRef();

    /*
    Store
    */
    const dataStore = useDataStore();

    const history = useHistory();

    let menuClick = false;

    useEffect(() => {
        if (sidebarActive) {
            addClass(document.body, "body-overflow-hidden");
        } else {
            removeClass(document.body, "body-overflow-hidden");
        }
    }, [sidebarActive]);

    const onInputStyleChange = (inputStyle) => {
        setInputStyle(inputStyle);
    };

    const onRipple = (e) => {
        PrimeReact.ripple = e.value;
        setRipple(e.value);
    };

    const onLayoutModeChange = (mode) => {
        //console.log.log(mode);
        setLayoutMode(mode);
    };

    const onColorModeChange = (mode) => {
        setLayoutColorMode(mode);
    };

    const onWrapperClick = (event) => {
        if (!menuClick && layoutMode === "overlay") {
            setSidebarActive(false);
        }
        menuClick = false;
    };

    const onToggleMenu = (event) => {
        menuClick = true;

        setSidebarActive((prevState) => !prevState);

        event.preventDefault();
    };

    const onSidebarClick = () => {
        menuClick = true;
    };

    const onMenuItemClick = (event) => {
        if (!event.item.items && layoutMode === "overlay") {
            setSidebarActive(false);
        }
    };

    const menu = [
        {
            label: "Procesos",
            icon: "pi pi-fw pi-sitemap",
            items: [
                { label: "Crear Producto", icon: "pi pi-fw pi-id-card", to: "/producto" },
                { label: "Crear Orden", icon: "pi pi-fw pi-check-square", to: "/orden" },
                { label: "Listado de Odenes", icon: "pi pi-fw pi-list", to: "/listaOrden" },
            ],
        },
    ];

    const addClass = (element, className) => {
        if (element.classList) element.classList.add(className);
        else element.className += " " + className;
    };

    const removeClass = (element, className) => {
        if (element.classList) element.classList.remove(className);
        else element.className = element.className.replace(new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
    };

    const isSidebarVisible = () => {
        return sidebarActive;
    };

    const logo = layoutColorMode === "dark" ? "assets/layout/images/EdimcaLogo.jpg" : "assets/layout/images/EdimcaLogo.jpg";

    const wrapperClass = classNames("layout-wrapper", {
        "layout-overlay": layoutMode === "overlay",
        "layout-static": layoutMode === "static",
        "layout-active": sidebarActive,
        "p-input-filled": inputStyle === "filled",
        "p-ripple-disabled": ripple === false,
    });

    const sidebarClassName = classNames("layout-sidebar", {
        "layout-sidebar-dark": layoutColorMode === "dark",
        "layout-sidebar-light": layoutColorMode === "light",
    });
    const onLogout = () => {
        localStorage.removeItem("login");
        setIsLoggedIn(false); // Actualizar el estado de inicio de sesión
        dataStore.removeAuthPrincipalUser();
        history.push("/"); // Redirigir a la página de inicio de sesión (o a la ruta deseada)
    };
    return (
        <div className={wrapperClass} onClick={onWrapperClick}>
            <AppTopbar onToggleMenu={onToggleMenu} />
            <CSSTransition classNames="layout-sidebar" timeout={{ enter: 200, exit: 200 }} in={isSidebarVisible()} unmountOnExit>
                <div ref={sidebar} className={sidebarClassName} onClick={onSidebarClick}>
                    <div className="layout-logo" style={{ cursor: "pointer" }}>
                        <img alt="Logo" src={logo} style={{ width: "100px" }} />
                    </div>
                    <AppProfile onLogout={onLogout} />
                    <AppMenu model={menu} onMenuItemClick={onMenuItemClick} />
                </div>
            </CSSTransition>
            {/* <AppConfig rippleEffect={ripple} onRippleEffect={onRipple} inputStyle={inputStyle} onInputStyleChange={onInputStyleChange} layoutMode={layoutMode} onLayoutModeChange={onLayoutModeChange} layoutColorMode={layoutColorMode} onColorModeChange={onColorModeChange} />*/}
            <div className="layout-main">
                <Route exact path="/producto" component={Producto} />
                <Route exact path="/orden" component={Orden} />
                <Route exact path="/listaOrden" component={ListaOrden} />
            </div>
        </div>
    );
};
